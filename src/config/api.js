import axios from "axios";

const API = axios.create({
  baseURL: 'https://test.besnikselimi.com/api/',
});


export default API;
