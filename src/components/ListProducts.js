import Product from "./Product";

function ListProducts({products}) {
  return (
    <div className="row">
      {products.map(product => <Product key={product.id} product={product}/>)}
    </div>
  );
}

export default ListProducts;
