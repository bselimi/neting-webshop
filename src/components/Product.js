import {Link, useRouteMatch} from "react-router-dom";
import {formatEuro} from "../util/fomat-euro";
import './Product.css';
import {useState} from "react";
import {useWishList} from "../context/wishlist-context";
// import classNames from 'classnames';

function Product({product}) {
  let { url } = useRouteMatch();

  const { isOnWishlist, addToWishlist } = useWishList();

  // const [hover, setHover] = useState(false);
  const [showGallery, setShowGallery] = useState(false);
  // const classes = classNames('card', { shadow: hover });
  // const classes = 'card' + (hover ? ' shadow' : '');
  const [index, setIndex] = useState(0);

  const gallery = [
    product.image,
    product.image,
    product.image,
  ];

  const prevPicture = () => {
    if (index > 0) setIndex(index-1);
  }

  const nextPicture = () => {
    if (index < gallery.length - 1) setIndex(index+1);
  }

  const displayGallery = () => {
    setShowGallery(true);
    setIndex(0);
  }

  return (
    <div className="col-12 col-md-6 col-lg-4 col-xl-3 p-3 product">
      <div className="card" style={{width: '100%'}}
           // onMouseEnter={() => setHover(true)}
           // onMouseLeave={() => setHover(false)}
      >
        <div className="p-3">
          {showGallery && (
            <span className="close-gallery" onClick={() => setShowGallery(false)}>
              <i className="fas fa-times"/>
            </span>)
          }
          {!showGallery && <Link to={url + '/' + product.id}>
            <img className="card-img-top" src={product.image} alt="Card cap"/>
          </Link>}
          {showGallery && (
            <div className="d-flex align-items-center justify-content-center">
              <i className="fas fa-arrow-left" onClick={prevPicture}/>
              <img className="flex-shrink-1" src={gallery[index]} alt="Card cap"/>
              <i className="fas fa-arrow-right" onClick={nextPicture}/>
              <span className="position-absolute" style={{top: 0}}>{index + 1}</span>
            </div>
          )}
        </div>
        {!showGallery && (
          <div className="view-gallery">
            <span onClick={displayGallery}>Shiko gallerine</span>
          </div>
          )
        }
        <div className="card-body">
          <p className="card-text">{product.name}</p>
          {/*<p className="card-text">{product.id}: {product.description}</p>*/}
          <h5 className="card-title">{formatEuro(product.price)}</h5>
          <div className="d-flex">
            <Link to={url + '/' + product.id} className="btn btn-primary">
              Shiko detajet
            </Link>
            <button onClick={() => addToWishlist(product)} className="btn ml-2 btn-outline-primary">
              {isOnWishlist(product) && <i className="fas fa-heart"/>}
              {!isOnWishlist(product) && <i className="far fa-heart"/>}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Product;
