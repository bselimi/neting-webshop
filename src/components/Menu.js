import {useState} from "react";
import { NavLink } from "react-router-dom";
import './Menu.css';
import {useCategories} from "../context/categories-context";

function Menu() {
  const [selectedCategory, setSelectedCategory] = useState(null);
  const categories = useCategories();

  return (
    <div className="categories-menu mb-3 shadow-sm">
      <nav className="container nav flex-nowrap">
        {categories.map(cat => {
            const {slug, name} = cat;
            return (
              <div key={slug}
                   className="nav-item d-flex flex-column"
                   onMouseEnter={() => setSelectedCategory(cat)}
                   onMouseLeave={() => setSelectedCategory(null)}
              >
                <NavLink
                  className="nav-link flex-grow-1 d-flex flex-column justify-content-center"
                  activeClassName="active"
                  to={'/' + slug}>
                  <span>{name}</span>
                </NavLink>
                <div className="submenu position-absolute">
                  <div className="p-3 bg-white shadow container">
                    {cat.sub_categories.map(({slug, name}) =>
                      <NavLink activeClassName="active" key={slug} to={'/' + cat.slug + '/' + slug}>{name}</NavLink>
                    )}
                  </div>
                </div>
              </div>
            );
          }
        )}
        <NavLink className="nav-link" activeClassName="active" to='/cka-te-re'>Çka ka t're?</NavLink>
      </nav>
      {selectedCategory && <div className="backdrop position-absolute"></div> }
    </div>
  );
}

export default Menu;
