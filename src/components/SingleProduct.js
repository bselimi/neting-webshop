import {Link, useParams} from "react-router-dom";
import {formatEuro} from "../util/fomat-euro";
import {useEffect, useState} from "react";
import API from "../config/api";

function SingleProduct({category, subcategory}) {
  let { id } = useParams();

  const [product, setProduct] = useState(null);

  useEffect(() => {
    setProduct(null);
    API.get('products/' + id)
      .then(result => result.data)
      .then(data => setProduct(data));
  }, [id]);

  if (!product) {
    return <div>Loading...</div>;
  }

  return (
    <div className="d-flex">
      <div className="images">
        <img src={product.image} alt=""/>
      </div>
      <div className="data flex-grow-1">
        <h2>{product.name}</h2>
        <h4>{formatEuro(product.price)}</h4>
        <Link to={'/' + category.slug + (subcategory ? '/' + subcategory.slug : '')}>Back</Link>
      </div>
    </div>
  );
}

export default SingleProduct;
