import ListProducts from "./ListProducts";
import {useEffect, useState} from "react";
import API from "../config/api";

function SingleSubCategory({category, subcategory}) {
  const [products, setProducts] = useState(null);

  useEffect(() => {
    setProducts(null);
    API.get('products/sub/' + subcategory.id)
      .then(result => result.data)
      .then(data => setProducts(data));
  }, [subcategory]);

  return (
    <>
      <h3>Category: {category.name}, Subcategory: {subcategory.name}</h3>
      {!products && <div>Loading...</div>}
      {products && <ListProducts products={products}/>}
    </>
  );
}

export default SingleSubCategory;
