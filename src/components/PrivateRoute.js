import {
  Route,
  Redirect,
} from "react-router-dom";
import {useUser} from "../context/user-context";

function PrivateRoute({path, children, component, ...rest}) {
  const {loggedIn} = useUser();

  if (!loggedIn) {
    return <Redirect to="/login" />;
  }

  return (
    <Route {...rest}>
      {children}
    </Route>
  );
}

export default PrivateRoute;
