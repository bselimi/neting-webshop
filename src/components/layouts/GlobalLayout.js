import Header from "../Header";
import Footer from "../Footer";

function GlobalLayout({children}) {
  return (
    <div className="App d-flex flex-grow-1 flex-column">
      <Header/>
      {children}
      <Footer/>
    </div>
  )
}

export default GlobalLayout;
