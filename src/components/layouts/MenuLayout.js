import Menu from "../Menu";

function MenuLayout({children}) {
  return (
    <>
      <Menu />
      <main className="flex-grow-1">
        <section>
          {children}
        </section>
      </main>
    </>
  );
}

export default MenuLayout;
