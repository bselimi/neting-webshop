import './Header.css';
import logo from '../assets/logo.png';
import {useUser} from "../context/user-context";
import {Link} from "react-router-dom";
import {useWishList} from "../context/wishlist-context";
import {useState, useRef} from "react";
import useOnClickOutside from "../hooks/useOnClickOutside";


function WishListButton() {
  const ref = useRef();
  const {wishlist, removeFromWishlist} = useWishList();
  const [open, setOpen] = useState(false);
  useOnClickOutside(ref, () => setOpen(false));

  const iconClick = (e) => {
    setOpen(!open);
    if (open) {
      e.stopPropagation();
    }
  }

  return (
    <div ref={ref} className="wishlist d-inline-block position-relative">
      <button className="btn btn-link" onClick={e => iconClick(e)}>
        <i className="far fa-heart"/>
        {wishlist.length > 0 && <span className="badge badge-pill badge-warning">{wishlist.length}</span>}
      </button>
      {open && <div className="position-absolute bg-white text-black-50 p-2 shadow popup">
        {wishlist.map(product => (
          <div key={product.id} className="d-flex border-bottom p-2">
            <img alt="product" src={product.image} height={28}/>
            <div className="flex-grow-1 text-nowrap">{product.name}</div>
            <button className="ml-2 btn btn-link" onClick={() => removeFromWishlist(product)}>
              <i className="fa fa-times text-black-50"/>
            </button>
          </div>
        ))}
        <button className="btn btn-secondary">Beje porosine</button>
      </div>}
    </div>
  )
}

function Header() {
  const {loggedIn, logout} = useUser();

  return (
    <header className="top-header">
      <div className="container d-flex flex-column flex-md-row justify-content-between">
        <div className="logo p-2">
          <img src={logo} alt="" />
        </div>
        <div className="d-none d-md-block flex-grow-1"/>
        <div className="search p-2">
          <div className="input-group">
            <input type="text" className="form-control bg-dark border-0" placeholder="Recipient's username"
                   aria-label="Recipient's username" aria-describedby="button-addon2"/>
            <div className="input-group-append">
              <button className="btn btn-link" type="button" id="button-addon2">
                <i className="fas fa-search"/>
              </button>
            </div>
          </div>
        </div>
        <div className="d-none d-md-block flex-grow-1"/>
        <div className="icons p-2">
          {loggedIn && (
            <button className="btn btn-link" onClick={logout}>
              <i className="fas fa-user"/>
              Logout
            </button>
          )}
          {!loggedIn && (
            <Link to="/login" className="btn btn-link">
              <i className="fas fa-user"/>
              Login
            </Link>
          )}
          <button className="btn btn-link">
            <i className="fab fa-opencart"/>
          </button>
          <WishListButton/>
        </div>
      </div>
    </header>
  );
}

export default Header;
