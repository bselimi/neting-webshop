import {useUser} from "../context/user-context";
import {useState} from "react";
import {useHistory} from "react-router-dom";


function LoginForm() {
  let history = useHistory();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const {login} = useUser();

  const submitForm = async () => {
    login({email, password})
      .then(() => {
        if (history.length > 0) {
          history.push('/');
        }
      });
  }

  return (
    <div className="d-flex justify-content-center my-5 py-5">
      <div className="col-4 border-dark">
        <div className="form-group">
          <label htmlFor="exampleInputEmail1">Email address</label>
          <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                 placeholder="Enter email"
                 value={email} onChange={e => setEmail(e.target.value)}/>
          <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div className="form-group">
          <label htmlFor="exampleInputPassword1">Password</label>
          <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password"
                 value={password} onChange={e => setPassword(e.target.value)}/>
        </div>
        <button type="button" className="btn btn-primary" onClick={submitForm}>Submit</button>
      </div>
    </div>
  );
}

export default LoginForm;
