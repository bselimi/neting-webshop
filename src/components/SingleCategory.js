import ListProducts from "./ListProducts";
import {useEffect, useState} from "react";
import API from "../config/api";

function SingleCategory({category}) {
  const [products, setProducts] = useState(null);

  useEffect(() => {
    setProducts(null);
    API.get('products/cat/' + category.id)
      .then(result => result.data)
      .then(data => setProducts(data));
  }, [category]);

  return (
    <>
      <h3>Single category page: {category.name}</h3>
      {!products && <div>Loading...</div>}
      {products && <ListProducts products={products}/>}
    </>
  );
}

export default SingleCategory;
