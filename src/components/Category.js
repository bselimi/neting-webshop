import {Route, Switch, useRouteMatch} from "react-router-dom";
// import NewItems from "./NewItems";
import SingleCategory from "./SingleCategory";
import SingleSubCategory from "./SingleSubCategory";
import SingleProduct from "./SingleProduct";

function Category({category}) {
  const { url } = useRouteMatch();
  const { slug } = category;

  return <div className="container">
    {/*<nav className="nav">
    {category.subCategories.map(({slug, name}) =>
      <NavLink className="nav-link" activeClassName="active" key={slug} to={url + '/' + slug}>{name}</NavLink>
    )}
    </nav>*/}

    <Switch>
      <Route exact path={url}>
        <SingleCategory category={category} />
      </Route>
      {category.sub_categories.map(subcategory =>
        <Route exact key={subcategory.id} path={url + '/' + subcategory.slug}>
          <SingleSubCategory category={category} subcategory={subcategory} />
        </Route>
      )}
      {category.sub_categories.map(subcategory =>
        <Route key={subcategory.id + '-product'} path={url + '/' + subcategory.slug + '/:id'}>
          <SingleProduct category={category} subcategory={subcategory} />
        </Route>
      )}
      <Route path={url + '/:id'}>
        <SingleProduct category={slug} />
      </Route>
    </Switch>
  </div>
}

export default Category;
