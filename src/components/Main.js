import { Switch, Route } from "react-router-dom";
import NewItems from "./NewItems";
import Home from "./Home";
import Category from "./Category";
import {useCategories} from "../context/categories-context";
import Menu from "./Menu";
import LoginForm from "./LoginForm";
import Profile from "./Profile";
import PrivateRoute from "./PrivateRoute";


function Main() {
  const categories = useCategories();

  return (
    <Switch>
      <Route path="/login">
        <LoginForm/>
      </Route>
      <PrivateRoute path="/profile">
        <Profile/>
      </PrivateRoute>
      <Route path="/">
        <Menu />
        <main className="flex-grow-1">
          <section>
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              {categories.map(category => {
                const {id, slug} = category;
                return (
                  <Route key={id} path={'/' + slug}>
                    <Category category={category} />
                  </Route>
                );
              })}
              <Route path="/cka-te-re">
                <NewItems />
              </Route>
              <Route path="*">
                <button className="btn btn-primary">My button</button>
              </Route>
            </Switch>
          </section>
        </main>
      </Route>
    </Switch>
  );
}

export default Main;
