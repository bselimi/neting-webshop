function Example({ a, children }) {
  return (
    <div>
      <p>{a}</p>
      {children}
    </div>
  );
}

const jsx = (
  <Example a="Example prop">
    <h1>Test</h1>
    <div>Rest of children</div>
  </Example>
);

console.log(jsx)
