import Home from "../components/Home";
import MenuLayout from "../components/layouts/MenuLayout";

function HomePage() {
  return (
    <MenuLayout>
      <Home />
    </MenuLayout>
  );
}

export default HomePage;
