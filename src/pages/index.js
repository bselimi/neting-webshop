import Main from "../components/Main";
import GlobalLayout from "../components/layouts/GlobalLayout";
import {Route, Switch} from "react-router-dom";
import PrivateRoute from "../components/PrivateRoute";
import Profile from "../components/Profile";
import Menu from "../components/Menu";
import Category from "../components/Category";
import NewItems from "../components/NewItems";
import LoginFormPage from "./login-form";
import HomePage from "./HomePage";
import CategoryPage from "./CategoryPage";
import {useCategories} from "../context/categories-context";
import NewItemsPage from "./NewItemsPage";
import ProfilePage from "./ProfilePage";



function Pages() {
  const categories = useCategories();

  const categoryPages = categories.map(category => {
    const {slug} = category;
    return {
      path: '/' + slug,
      children: <CategoryPage category={category} />,
    };
  })

  const pages = [
    {path: '/', component: HomePage, exact: true},
    {path: '/login', component: LoginFormPage},
    {path: '/profile', component: ProfilePage},
    {path: '/cka-te-re', component: NewItemsPage},
    ...categoryPages
  ];

  return (
    <GlobalLayout>
      <Switch>
        {pages.map(page => {
          const {path, component, exact, children} = page;
          return <Route key={path} path={path} component={component} exact={exact}>{children}</Route>;
        })}
      </Switch>

    </GlobalLayout>
  );
}

export default Pages;
