import MenuLayout from "../components/layouts/MenuLayout";
import Profile from "../components/Profile";

function ProfilePage() {
  return (
    <MenuLayout>
      <Profile />
    </MenuLayout>
  );
}

export default ProfilePage;
