import Category from "../components/Category";
import MenuLayout from "../components/layouts/MenuLayout";

function CategoryPage({ category }) {
  return (
    <MenuLayout>
      <Category category={category} />
    </MenuLayout>
  );
}

export default CategoryPage;
