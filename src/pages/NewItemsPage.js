import Menu from "../components/Menu";
import NewItems from "../components/NewItems";
import MenuLayout from "../components/layouts/MenuLayout";
import Category from "../components/Category";

function NewItemsPage() {
  return (
    <MenuLayout>
      <NewItems />
    </MenuLayout>
  );
}

export default NewItemsPage;
