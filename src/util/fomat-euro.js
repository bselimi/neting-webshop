export function formatEuro(val) {
  return val.toFixed(2) + '  €';
}
