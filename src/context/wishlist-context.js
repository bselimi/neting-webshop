import {createContext, useContext, useState} from "react";
import {useEffect} from "react";
import API from "../config/api";
// import API from "../config/api";

const json = localStorage.getItem('wishlist') || null;
const initialWishlist = json ? JSON.parse(json) : [];

// Create context
const WishListContext = createContext({
  wishlist: initialWishlist,
  addToWishlist: () => {},
  isOnWishlist: () => {},
  removeFromWishlist: () => {},
});

// Create a shorthand function for using the context
export const useWishList = () => useContext(WishListContext);

// Create a context provider component so we can manage the provider's state
export function WishListProvider({children}) {
  const [wishlist, setWishlistInternal] = useState([]);

  useEffect(() => {
    API.get('products/list/' + initialWishlist.join(","))
      .then(result => result.data)
      .then(products => {
        const sorted = initialWishlist.map(id => products.find(it => it.id === id));
        setWishlistInternal(sorted);
      })
  }, []);

  const setWishlist = products => {
    setWishlistInternal(products);
    const value = JSON.stringify(products.map(it => it.id));
    localStorage.setItem('wishlist', value);
  }
  const find = p => wishlist.find(it => it.id === p.id);

  const addToWishlist = (product) => {
    if (!find(product)) {
      setWishlist([...wishlist, product]);
    }
  };

  const isOnWishlist = (product) => {
    return Boolean(find(product));
  };

  const removeFromWishlist = (product) => {
    setWishlist(wishlist.filter(it => it.id !== product.id));
  };

  const value = {
    wishlist,
    addToWishlist,
    isOnWishlist,
    removeFromWishlist,
  };

  return (
    <WishListContext.Provider value={value}>
      {children}
    </WishListContext.Provider>
  );
}


