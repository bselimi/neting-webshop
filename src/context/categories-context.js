import {createContext, useContext, useEffect, useState} from "react";
import API from "../config/api";

// Create context
const CategoriesContext = createContext(null);

// Create a shorthand function for using the context
export const useCategories = () => useContext(CategoriesContext);

// Create a context provider component so we can manage the provider's state
export function CategoriesProvider({children}) {
  const [categories, setCategories] = useState(null);
  // Take categories from the API
  useEffect(() => {
    API.get('categories')
      .then(result => {
        setCategories(result.data);
      });
  }, []);

  if (!categories) {
    return <div>Loading...</div>
  }

  return (
    <CategoriesContext.Provider value={categories}>
      {children}
    </CategoriesContext.Provider>
  );
}


