import {createContext, useContext, useEffect, useState, useCallback} from "react";
import API from "../config/api";



const token = localStorage.getItem('id_token') || null;

const DEFAULT_USER = {
  loggedIn: Boolean(token),
  token,
  user: null,
  login: () => {},
  logout: () => {},
}


// Create context
const UserContext = createContext(DEFAULT_USER);

// Create a shorthand function for using the context
export const useUser = () => useContext(UserContext);

// Create a context provider component so we can manage the provider's state
export function UserProvider({children}) {
  const [user, setUser] = useState(DEFAULT_USER);

  const login = useCallback(({email, password}) => {
    // API call to login the user
    return API.post('login', {email, password})
      .then(response => response.data)
      .then(data => {
        const token = data.access_token;
        localStorage.setItem('id_token', token);
        setUser(s => ({
          ...s,
          loggedIn: Boolean(token),
          token,
        }));
        return true;
      })
  }, []);

  const logout = useCallback(() => {
    localStorage.removeItem('id_token');
    setUser(s => ({
      ...s,
      loggedIn: false,
      token: null,
    }));
  }, []);

  useEffect(() => {
    // Response interceptor for API calls
    API.interceptors.response.use((response) => {
      return response
    }, async function (error) {
      // const originalRequest = error.config;
      if (error.response.status === 401) {
        logout();
      }
      return Promise.reject(error);
    });
  }, [logout]);

  const contextValue = {
    ...user,
    login,
    logout,
  };

  return (
    <UserContext.Provider value={contextValue}>
      {children}
    </UserContext.Provider>
  );
}


