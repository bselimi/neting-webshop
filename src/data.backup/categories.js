const categories = [
  {slug: 'kompjutere-laptop', name: 'Kompjuterë, laptopë', subCategories: [
      {slug: 'aksesore', name: 'Aksesorë'},
      {slug: 'kompjutere', name: 'Kompjuterë'},
      {slug: 'laptope', name: 'Laptopë'},
  ]},
  {slug: 'celular-tablet-pajisje-smart', name: 'Celularë, tabletë dhe pajisje smart', subCategories: [
      {slug: 'cellulare', name: 'Cellularë'},
      {slug: 'tabler', name: 'Tablet'},
    ]},
  {slug: 'gaming', name: 'Gaming', subCategories: [
      {slug: 'aksesore', name: 'Aksesorë'},
      {slug: 'kompjutere', name: 'Kompjuterë'},
      {slug: 'laptope', name: 'Laptopë'},
    ]},
  {slug: 'aparate-fotografike', name: 'Aparate fotografike', subCategories: [
      {slug: 'aksesore', name: 'Aksesorë'},
      {slug: 'kompjutere', name: 'Kompjuterë'},
      {slug: 'laptope', name: 'Laptopë'},
    ]},
  {slug: 'tv-video-audio', name: 'TV, video dhe audio', subCategories: [
      {slug: 'aksesore', name: 'Aksesorë'},
      {slug: 'kompjutere', name: 'Kompjuterë'},
      {slug: 'laptope', name: 'Laptopë'},
    ]},
  {slug: 'pjese-kompjutere', name: 'Pjesë për kompjuterë', subCategories: [
      {slug: 'aksesore', name: 'Aksesorë'},
      {slug: 'kompjutere', name: 'Kompjuterë'},
      {slug: 'laptope', name: 'Laptopë'},
    ]},
  {slug: 'aksesore', name: 'Aksesorë', subCategories: [
      {slug: 'aksesore', name: 'Aksesorë'},
      {slug: 'kompjutere', name: 'Kompjuterë'},
      {slug: 'laptope', name: 'Laptopë'},
    ]},
  {slug: 'outlet', name: 'Outlet', subCategories: [
      {slug: 'aksesore', name: 'Aksesorë'},
      {slug: 'kompjutere', name: 'Kompjuterë'},
      {slug: 'laptope', name: 'Laptopë'},
    ]},
];

export default categories;
