import './App.css';
import Main from "./components/Main";
import Footer from "./components/Footer";
import Header from "./components/Header";
import {
  BrowserRouter as Router,
} from "react-router-dom";
import {CategoriesProvider} from "./context/categories-context";
import {UserProvider} from "./context/user-context";
import {WishListProvider} from "./context/wishlist-context";
import Pages from "./pages";

function App() {
  return (
    <Router>
      <UserProvider>
        <CategoriesProvider>
          <WishListProvider>
            <Pages/>
          </WishListProvider>
        </CategoriesProvider>
      </UserProvider>
    </Router>
  );
}

export default App;
